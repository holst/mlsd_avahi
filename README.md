# Documentation

## Avahi

*   Origin [README](README)
*   Homepage: <http://avahi.org>
*   Prebuilt doxygen: <http://avahi.org/download/doxygen/>
*   Some informations are located in *doc* directory (incl. dia graphics)
*   Misc [notes](notes.md)

## mlsd

*   Service Discovery Library
*   Based on [Kaiser2014Stateless](https://netfuture.ch/2014/12/stateless-dns/)
*   [Repository](https://gitlab.com/holst/mlsd)

# Build

If this repository already cloned the following build/install steps can be automated with
~~~bash
./mlsd-bootstrap.sh
~~~
Then continue at the next section.

*   Clone with submodules:
    ~~~bash
    git clone --recursive https://gitlab.com/holst/mlsd_avahi.git
    ~~~
    Or initialize the submodules separately:
    ~~~bash
    git clone https://gitlab.com/holst/mlsd_avahi.git
    cd mlsd_avahi
    git submodule init
    git submodule update
    ~~~

*   Build the mlsd-library:
    ~~~bash
    make -C mlsd
    ~~~

*   Configure avahi with minimal requirements and installation path *$PWD/local*
    ~~~bash
    env NOCONFIGURE=1 ./bootstrap.sh
    ./configure \
        --enable-stateless \
        --with-distro=none \
        --disable-autoipd \
        --disable-monodoc \
        --disable-gdbm \
        --disable-mono \
        --disable-nls \
        --disable-mono \
        --disable-qt3 \
        --disable-qt4 \
        --disable-gtk3 \
        --disable-python \
        --disable-manpages \
        --disable-xmltoman \
        --with-avahi-user=$USER \
        --with-avahi-group=$(id -gn) \
        --prefix=$PWD/local \
        --sbindir=$PWD/local/bin \
        --with-systemdsystemunitdir=$PWD/local/systemd
    ~~~

*   Build and install avahi to *$PWD/local*
    ~~~bash
    make
    make install
    ~~~

# Run avahi + mlsd

*   Make sure, the first name server in */etc/resolv.conf* is valid.

*   Set the *allow-interfaces* variable in *$PWD/local/etc/avahi/avahi-daemon.conf* to
    an interface, which is reachable from the name server (line 27).

*   Start the daemon enough privileges to bind port 53 and disable the chroot behavior, e.g.
    ~~~bash
    sudo local/bin/avahi-daemon --no-chroot
    ~~~

# Verification

*   Manual, e.g.:
    ~~~bash
    dig PTR _services._dns-sd._udp.ssdisc.com
    ~~~

*   avahi-browse (Build with qt or use preexisting one)
    ~~~bash
    avahi-browse -ar
    ~~~

*   avahi-discover (Build with gtk or use preexisting one)
    ~~~bash
    avahi-discover
    ~~~
