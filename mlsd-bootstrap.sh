#!/bin/bash

# Update mlsd submodule
git submodule init
git submodule update

# Build submodule
make -C mlsd

# Configure avahi
env NOCONFIGURE=1 ./bootstrap.sh
env ./configure \
      --enable-stateless \
      --with-distro=none \
      --disable-autoipd \
      --disable-monodoc \
      --disable-gdbm \
      --disable-mono \
      --disable-nls \
      --disable-mono \
      --disable-qt3 \
      --disable-qt4 \
      --disable-gtk3 \
      --disable-python \
      --disable-manpages \
      --disable-xmltoman \
      --with-avahi-user=$USER \
      --with-avahi-group=$(id -gn) \
      --prefix=$PWD/local \
      --sbindir=$PWD/local/bin \
      --with-systemdsystemunitdir=$PWD/local/systemd

# Build and install avahi
make
make install
