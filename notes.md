# Notes

Some development notes about Avahi.

## avahi-common

Useful functions for custom clients and the avahi-daemon

### simple-watch.\{c,h\}

*   A simple event loop using **poll()** to dispatching various file descriptors
    (AvahiPoll)
*   A wakeup fd implemented as pipe to handle unexpected wakeups (wakeup_pipe)
*   Timeouts dispatching: Only poll until the next timeout

### thread-watch.\{c,h\}

*   A mutex protected simple-watch in an own thread

## avahi-core

Implementation of the mDNS stack.

### server.c

*   Use the simple event loop

### timeeventq.\{c.h\}

*   Manage time events
*   Always dispatch the earliest one with the simple event loop

## avahi-daemon

*   Put together avahi-core and avahi-common to daemonize the mDNS stack and
    make it accessible through dbus and a simplified protocol
    (simple-protocol.\{c,h\}).
*   Configurable over xml-configurations per service (static-services.\{c,h\})
*   Security:
    +   chroot/chown
    +   drop capabilities

# Build environment (minimal)

*   bootstrap(automake), configure, build and install. Build with avahi
    user/group as your own id to easier debugging.


    > Note: *--disable-nls* doesn't seem to work. Therefore disable all
    > languages manually with `sed -e 's/.*/#&/g' po/LINGUAS`{.bash}
    > to prevent language file generation.

    ~~~bash
    env NOCONFIGURE=1 ./bootstrap.sh
    env \
      CFLAGS='-O0 -g' \
      ./configure \
      --with-distro=none \
      --disable-autoipd \
      --disable-monodoc \
      --disable-nls \
      --disable-dbus \
      --disable-mono \
      --disable-qt3 \
      --disable-qt4 \
      --disable-python \
      --disable-manpages \
      --disable-xmltoman \
      --with-avahi-user=$USER \
      --with-avahi-group=$(id -gn) \
      --prefix=$PWD/local \
      --sbindir=$PWD/local/bin
    make
    make install
    ~~~

*   To run the tests and examples, append *--enable-tests* to the configure
    script. Run the tests with `make check`{.bash}

*   Debugging: `gdb --args $PWD/local/bin/avahi-daemon --no-drop-root
    --no-chroot`{.bash}
