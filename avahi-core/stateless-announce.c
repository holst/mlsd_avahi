#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <avahi-common/malloc.h>
#include <mlsd/src/stateless.h>

#include "internal.h"
#include "log.h"
#include "stateless-announce.h"

static void record_to_str(char* buf, size_t len, AvahiRecord* r) {
    char* p;
    char* key_str;
    size_t n;

    assert(buf);
    assert(r);

    /* Get Key Class Type Ttl */
    key_str = avahi_key_to_string(r->key);
    n = snprintf(buf, len, "%s  ", key_str);
    p = buf+n;
    len -= n;
    avahi_free(key_str);


    /* Get record data as string */
    avahi_record_data_to_string(p, len, r);
    n = strlen(p);
    if (!n) {
        *p++ = '"';
        *p++ = '"';
    }
}

static void announce_walk_callback(AvahiInterfaceMonitor *m, AvahiInterface *i, void* userdata) {
    char addr_buf[AVAHI_ADDRESS_STR_MAX];
    AvahiEntry* e = userdata;
    AvahiServer* s = m->server;
    (void)s;

    assert(m);
    assert(i);
    assert(e);
    assert(!e->dead);

    if (!avahi_interface_match(i, e->interface, e->protocol) || !avahi_entry_is_commited(e))
        return;

    if (i->mcast_joined && !avahi_address_snprint(addr_buf, AVAHI_ADDRESS_STR_MAX, &i->local_mcast_address)) {
        avahi_log_error("Can't generate string from address");
        return;
    }

    stateless_server_interface_add(s->stateless_server, addr_buf);
}

void avahi_stateless_announce_entry(AvahiServer* s, AvahiEntry* e) {
    assert(s);
    assert(e);
    assert(!e->dead);

    /* printf("Don't announce single entry, announce group...\n"); */
}

void avahi_stateless_reannounce_entry(AvahiServer *s, AvahiEntry *e) {
    assert(s);
    assert(e);
    printf("Reannounce...\n");
}

void avahi_stateless_goodbye_entry(AvahiServer *s, AvahiEntry *e, int send_goodbye, int remove) {
    assert(s);
    assert(e);
    (void)send_goodbye;
    (void)remove;

    printf("Say goodbye\n");
}

void avahi_stateless_announce_group(AvahiServer *s, AvahiSEntryGroup *g) {
    AvahiEntry *e;
    /* char* subdomain; */
    char buf[1024];

    assert(s);
    assert(g);

    for (e = g->entries; e; e = e->by_group_next) {
        if (!e->dead) {
            if (e->interface >= 0) {
                avahi_interface_monitor_walk(s->monitor, e->interface, e->protocol, announce_walk_callback, e);
            }

            /* Get Key information */
            record_to_str(buf, 1024, e->record);
            printf("%s\n", buf);

            stateless_server_record_add(s->stateless_server, buf);

            /* free(subdomain); */
        }
    }
}

void avahi_stateless_check_group_probed(AvahiSEntryGroup *g, int immediately) {
    assert(g);
    assert(!g->dead);

    (void)immediately;

    /* printf("Establish group\n"); */
    avahi_s_entry_group_change_state(g, AVAHI_ENTRY_GROUP_ESTABLISHED);
}
